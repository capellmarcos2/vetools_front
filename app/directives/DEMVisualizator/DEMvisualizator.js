/**
 * Created by alex on 2/11/16.
 */
/**
 * Created by alex on 5/9/16.
 */
angular.module('directives.demVisualizator', [])
    .directive('demVisualizator', dcboxListDirective);

dcboxListDirective.$inject = ['$timeout'];

function dcboxListDirective($timeout) {
    return {
        restrict:'E',
        replace:true,
        templateUrl:'app/directives/DEMVisualizator/DEMvisualizator.html',
        scope: {

        },
        link: function($scope, iElement, iAttrs, controller) {
            require([
                    "esri/Map",
                    "esri/views/MapView",
                    "esri/layers/MapImageLayer",
                    "dojo/on",
                    "dojo/domReady!"
                ],
                function(
                    Map, MapView, MapImageLayer, on
                ) {

                    var layer = new MapImageLayer({
                        url: "https://sampleserver6.arcgisonline.com/arcgis/rest/services/Elevation/GlacierBay/MapServer",
                        minScale: 2000000,
                        sublayers: [{
                            id: 1,
                            title: "hillshade",
                            source: {
                                // indicates the source of the sublayer is a dynamic data layer
                                type: "data-layer",
                                // this object defines the data source of the layer
                                // in this case it's a raster located in a registered workspace
                                dataSource: {
                                    type: "raster",
                                    workspaceId: "GlacierBayID",
                                    dataSourceName: "gb_hillshade"
                                }
                            }
                        }, {
                            id: 0,
                            title: "dem"
                        }]
                    });

                    var map = new Map({
                        basemap: "hybrid",
                        layers: [layer]
                    });

                    var view = new MapView({
                        container: "viewDiv",
                        map: map,
                        center: [-136.897460, 58.496968],
                        zoom: 13
                    });

                // Change the layer's opacity value based on the value
                // of the slider
                layer.then(function() {
                    var slider = document.querySelector(".opacity-slider");
                    var opacity = document.querySelector(".opacity");
                    on(slider, "input", function() {
                        layer.opacity = slider.value;
                        opacity.innerText = slider.value;
                    });
                });

            });


        }
    };
}