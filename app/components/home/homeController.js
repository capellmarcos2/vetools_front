/**
 * Created by alexmarcos on 6/3/16.
 */
vetools.controller('homeController', function($scope,$state,NgMap) {

    $scope.slides = [];
    $scope.indexSlides=0;
    $scope.indexCsic=0;
    $scope.slides.push({
        image: 'assets/img/carrousel/img2.jpeg',
        title: 'VolcanBox',
        text: 'Development and Implementation of e-tools for volcanic hazard assessment and risk management',
        id:  $scope.indexSlides++
    });
    $scope.slides.push({
        image: 'assets/img/carrousel/img1.jpeg',
        title: 'Our goal',
        subtext: 'The main goal pursued by the proposal is to create an integrated software platform specially designed to assess and manage volcanic risk.',
        id:  $scope.indexSlides++
    });
    $scope.slides.push({
        image: 'assets/img/carrousel/img4.png',
        title: 'Our project',
        subtext: 'The project will facilitate interaction and cooperation between scientists and Civil Protection Agencies in order to share, unify, and exchange procedures, methodologies and technologies to effectively reduce the impacts of volcanic disasters by improving assessment and management of volcanic risk.',
        id:  $scope.indexSlides++
    });

    $scope.teamCsic = [];

    NgMap.getMap().then(function(map) {
        console.log(map.getCenter());
        console.log('markers', map.markers);
        console.log('shapes', map.shapes);
    });
});