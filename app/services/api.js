vetools.factory('vetoolsService', ['$http', '$rootScope', '$q','$http', function (http, rootScope, $q,$http) {


    var vtService = {};

    vtService.getMenu = function(cb){
        var url = "plugin/menu.json";

        $http.get(url).then(function (response) {
            cb(null,response);
        },function (error) {
            cb(error);
        });
    };


    vtService.callPlugin = function (url,parameters) {
        var deferred = $q.defer();
        var promise = deferred.promise;

        $http.post(url,parameters,{
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (val) {
            deferred.resolve(val);
        });


        return promise;
    };


    vtService.call = function (url, method, params, success, error) {
        var reqBody = {
            url: url,
            method: method,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            }
        };

        if (method === "GET") {
            reqBody.params = params;
        } else if (method === "POST") {
            reqBody.data = params;
        }

        http(reqBody).then(
            function (response) {
                success(response);
            },
            error
        );
    };

    return vtService;
}]);
