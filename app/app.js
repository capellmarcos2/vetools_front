/**
 * Created by alexmarcos on 6/3/16.
 */

"use strict";

var vetools = angular.module('vetools',
    [
        'ngRoute',
        'ngAnimate',
        'ngTouch',
        'ui.router',
        'ui.bootstrap',
        'ngMap',
        'ngRangeFilter',
        'bootstrapSubmenu',
        'ngSanitize',
        'angularModalService',
        'directives.demVisualizator',
        'directives.fileupload'
    ]
);





