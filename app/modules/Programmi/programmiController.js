/**
 * Created by alexmarcos on 6/3/16.
 */
vetools.controller('programmiController', function($scope,$state,$stateParams,$timeout,vetoolsService,utilService) {

    $scope.pluginSelected = $stateParams.namePlugin;


    //todo: create from directive
    $scope.openMenu = true;
    $scope.tab = 1;
    $("#linePointsPath").fileinput({showUpload: false});

    $scope.utils = utilService;

    $scope.clickRUN = function () {
        var fd = new FormData();
        $(".plugin-input").each(function (index,obj) {
            var jObj = $(obj);
            if(jObj.val() != "")
                fd.append(jObj.attr("id"),jObj.val());
        });
        $(".file-input").each(function (index,obj) {
            var jObj = $(obj);
            if(jObj.val() != "")
                fd.append(jObj.attr("id"), jObj.fileinput('getFileStack')[0]);
        });

        console.log(fd);
        var url = "";
        
        vetoolsService.callPlugin(url,fd).then(function (val) {
            console.log(val);
        })
    };

    $scope.toggleMenu = function(){
        if($(".content-menu").hasClass("open-menu")){
            $(".content-menu").removeClass("open-menu").css("opacity","0");
            $(".tabs").css("padding-left","0%");
            $("#pluginMenuView").css({"width":"0px"});
            $timeout(function(){
                $scope.openMenu=false;
            },500)
        }
        else{
            $(".content-menu").addClass("open-menu").css("opacity","1");
            $(".tabs").css("padding-left","20%");
            $("#pluginMenuView").css({"width":"20%"});
            $scope.openMenu=true;
        }
    }

});