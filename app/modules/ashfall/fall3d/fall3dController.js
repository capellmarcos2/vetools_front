vetools.controller('fall3dController', function($scope,$state,$stateParams,$timeout,vetoolsService,utilService) {
    $scope.pluginSelected = $stateParams.namePlugin;

    $scope.openMenu = true;
    $scope.tab = 1;

    $scope.hazmap = {
        mode: null,
        gridType: null,
        vmodel: null,
        ifvofz: null,
        massErupted: null,
        smodel: null,
        gridDimension: {
            x: 0,
            y:0
        },
        cellDimension: {
            x: 0,
            y: 0
        },
        ventCoord: {
            x:0,
            y:0,
            z:0
        },
        hmax: 0,
        vdSteps: 0,
        columnShape: {
            a: 0,
            lambda: 0
        },
        atmosphereTurbulenceCoeff: 0,
        nthr: {
            num: 0,
            values: []
        },
        npc: {
            num: 0,
            values: []
        },
        gridout: null,
        speout: null,
        vsetout: null,
        outformat: null
    };

    $scope.fall3d = {
        source: {
            type: ''
        }
    };

    $scope.selectTab = function (tab,$event) {
        $scope.tab = tab;
        $(".menu-options").removeClass("active");
        $($event.target).addClass("active");
    };

    $scope.toggleMenu = function(){
        if($(".content-menu").hasClass("open-menu")){
            $(".content-menu").removeClass("open-menu").css("opacity","0");
            $(".tabs").css("padding-left","0%");
            $("#pluginMenuView").css({"width":"0px"});
            $timeout(function(){
                $scope.openMenu=false;
            },500)
        }
        else{
            $(".content-menu").addClass("open-menu").css("opacity","1");
            $(".tabs").css("padding-left","20%");
            $("#pluginMenuView").css({"width":"20%"});
            $scope.openMenu=true;
        }
    }
});
