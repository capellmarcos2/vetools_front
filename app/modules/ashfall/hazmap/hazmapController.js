vetools.controller('hazmapController', function($scope,$state,$stateParams,$http,$timeout,vetoolsService,utilService, $uibModal) {
    $scope.pluginSelected = $stateParams.namePlugin;

    $scope.style = 'style="text-align:center; background-color: white; border-radius: 2vw; padding-bottom: 2vh;"';

    $scope.openMenu = true;
    $scope.tab = 1;

    $scope.hazmap = {
        mode: null,
        gridType: null,
        vmodel: null,
        ifvofz: null,
        massErupted: null,
        smodel: null,
        gridDimension: {
            x: 0,
            y:0
        },
        cellDimension: {
            x: 0,
            y: 0
        },
        ventCoord: {
            x:0,
            y:0,
            z:0
        },
        hmax: 0,
        vdSteps: 0,
        columnShape: {
            a: 0,
            lambda: 0
        },
        atmosphereTurbulenceCoeff: 0,
        nthr: {
            num: 0,
            values: []
        },
        npc: {
            num: 0,
            values: []
        },
        gridout: null,
        speout: null,
        vsetout: null,
        outformat: null
    };

    $scope.selectTab = function (tab,$event) {
        $scope.tab = tab;
        $(".menu-options").removeClass("active");
        $($event.target).addClass("active");
    };

    $scope.toggleMenu = function(){
        if($(".content-menu").hasClass("open-menu")){
            $(".content-menu").removeClass("open-menu").css("opacity","0");
            $(".tabs").css("padding-left","0%");
            $("#pluginMenuView").css({"width":"0px"});
            $timeout(function(){
                $scope.openMenu=false;
            },500)
        }
        else{
            $(".content-menu").addClass("open-menu").css("opacity","1");
            $(".tabs").css("padding-left","20%");
            $("#pluginMenuView").css({"width":"20%"});
            $scope.openMenu=true;
        }
    };

    $scope.clickRUN = function () {

        var inpString = "";

        inpString += $scope.hazmap.mode+"\n";
        inpString += $scope.hazmap.gridType+"\n";
        inpString += $scope.hazmap.ifvofz+"\n";
        inpString += $scope.hazmap.gridout+"\n";
        inpString += $scope.hazmap.speout+"\n";
        inpString += $scope.hazmap.vsetout+"\n";
        inpString += $scope.hazmap.outformat+"\n";
        if ($scope.hazmap.gridType == 0) {
            inpString += $scope.hazmap.gridDimension.x + " " + $scope.hazmap.gridDimension.y + "\n";
            inpString += $scope.hazmap.cellDimension.x + ".0 " + $scope.hazmap.cellDimension.y + ".0\n";
        }
        inpString += $scope.hazmap.massErupted.toExponential(1)+"\n";
        inpString += $scope.hazmap.smodel+"\n";
        inpString += $scope.hazmap.ventCoord.x+".0 "+$scope.hazmap.ventCoord.y+".0 "+$scope.hazmap.ventCoord.z+".0 "+"\n";
        inpString += $scope.hazmap.hmax+".0"+"\n";
        inpString += $scope.hazmap.vdSteps+"\n";
        inpString += $scope.hazmap.columnShape.a+".0 "+$scope.hazmap.columnShape.lambda+".0 "+"\n";
        inpString += $scope.hazmap.atmosphereTurbulenceCoeff+".0\n";
        inpString += $scope.hazmap.nthr.num+"\n";
        inpString += $scope.hazmap.nthr.values.join(". ")+".\n";
        inpString += $scope.hazmap.npc.num+"\n";
        for (var i=0; i<$scope.hazmap.npc.num; i++) {
            inpString += $scope.hazmap.npc.values[i].diameter.toExponential(1)
                +" "+ $scope.hazmap.npc.values[i].diameter.toExponential(1)
                +" "+ $scope.hazmap.npc.values[i].density
                +" "+ $scope.hazmap.npc.values[i].shape
                +" "+ $scope.hazmap.npc.values[i].wperc
                +"\n";
        }

        var fd = new FormData();
        fd.append('input', inpString);

        $scope.modal = $uibModal.open({
            template: '<div class="col-xs-12" '+$scope.style+'><h3>Running</h3> <span>Calculating inputs file...</span></div>'
        });
        $http.post("http://198.211.123.50/back/hazmap/hazmapinp.php", fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .success(function(data){
                $scope.simulation = 'idle';
                console.log('success', data);
                if (data.success) {
                    $scope.modal.close();
                    $scope.modal = $uibModal.open({
                        template: '<div class="col-xs-12" '+$scope.style+'><h3>Result</h3><span>Hazmap inputs generated</span></div>'
                    });

                    $timeout(function(){
                        $scope.modal.close();
                        $scope.modal = $uibModal.open({
                            template: '<div class="col-xs-12" '+$scope.style+'><h3>Running</h3> <span>Generating hazmap...</span></div>'
                        });
                        $http.post("http://198.211.123.50/back/hazmap/hazmap.php", fd, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        })
                            .success(function(data){
                                $scope.simulation = 'idle';
                                console.log('success', data);
                                if (data.success) {
                                    $scope.modal.close();
                                    $scope.modal = $uibModal.open({
                                        template: '<div class="col-xs-12" '+$scope.style+'><h3>Result</h3><span>Hazmap generated</span></div>'
                                    });
                                } else {
                                    $scope.modal.close();
                                    $scope.modal = $uibModal.open({
                                        template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3><span>Hazmap map generation failed - HAZMAP 102</span></div>'
                                    });
                                }
                            })
                            .error(function(err){
                                $scope.simulation = 'idle';
                                $scope.modal.close();
                                $scope.modal = $uibModal.open({
                                    template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3><span>Request has failed - Error 308</span></div>'
                                });
                                console.log('error', err);
                            });

                    }, 3000);


                } else {
                    $scope.modal.close();
                    $scope.modal = $uibModal.open({
                        template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3><span>Hazmap inputs generation failed - HAZMAP 101</span></div>'
                    });
                }
            })
            .error(function(err){
                $scope.simulation = 'idle';
                //alert("The simulation has failed. Error 308");
                $scope.modal.close();
                $scope.modal = $uibModal.open({
                    template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3><span>Request has failed - Error 308</span></div>'
                });
                console.log('error', err);
            });

    }

});