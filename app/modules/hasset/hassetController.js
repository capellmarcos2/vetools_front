/**
 * Created by alexmarcos on 6/3/16.
 */
vetools.controller('hassetController', function($scope,$state,$stateParams,$timeout,vetoolsService,utilService,$http) {

    $scope.pluginSelected = $stateParams.namePlugin;


    //todo: create from directive
    $scope.openMenu = true;
    $scope.tab = 1;
    $scope.result = false;
    $scope.scenarioResult = [];
    $("#linePointsPath").fileinput({showUpload: false});

    $scope.utils = utilService;
    $scope.csvData = [];
    $scope.selectTab = function (tab,$event) {
        $scope.tab = tab;
        $(".menu-options").removeClass("active");
        $($event.target).addClass("active");
    };
    $("#linePointsPath").on('fileloaded', function(event, file, previewId, index, reader) {
        var csvFile = $(this).fileinput('getFileStack')[0];
        reader.readAsText(file);
        reader.onload = function(event) {
            var csvData = event.target.result;
            $scope.csvData = $.csv.toArrays(csvData);
            console.log($scope.csvData);
            $scope.$apply();
        };
        reader.onerror = function() {
            alert('Unable to read ' + file.fileName);
        };

        console.log(event, file, previewId, index, reader,csvFile);
    });

    $scope.totalProvabilityEstimate = "";
    $scope.totalStandardDeviation = "";

    $scope.calculateSumProbability = function () {
        $scope.totalProvabilityEstimate = "";
        $scope.totalStandardDeviation = "";
        var sumVar =0;
        var sumProb =0;
        angular.forEach($scope.scenarioResult,function (value, index) {
            sumVar += parseFloat(value[2])*parseFloat(value[2]);
            sumProb +=parseFloat(value[1]);
        });
        $scope.totalProvabilityEstimate = Math.round(sumProb * 1000000) / 10000000;
        $scope.totalStandardDeviation = Math.round( Math.sqrt(sumVar) * 1000000)/1000000;
    };

    $scope.clickScenario = function () {
        console.log($scope.scenario);
        var url = "http://localhost:3333/server_vetools/hasset.php";
        $scope.csvSend = angular.copy($scope.csvData);
        $scope.csvSend.splice(0,1);
        for(var i=0;i<$scope.csvSend;i++){
            $scope.csvSend[i].splice(0,1);
        }
        var parameters = JSON.stringify(
            {'data':$scope.csvSend,
                'extra':{
                    'lineEditDataTW' : $scope.lineEditDataTW,
                    'lineEditProbTW' : $scope.lineEditProbTW,
                    'lineEditNTW' : $scope.lineEditNTW,
                    'scenario' : $scope.scenario
                }});

        $http.post(url+"?type=scenario",parameters).then(function (val) {
            var res = val.data;
            if(res['error']){
                alert(res['message'])
            }
            else{
                $scope.scenarioResult.push(res);
                $scope.calculateSumProbability();
            }
        });
    };
    $scope.downloadResult = function () {
        var url = "http://localhost:3333/server_vetools/hasset.php";
        $scope.csvSend = angular.copy($scope.csvData);
        $scope.csvSend.splice(0,1);
        for(var i=0;i<$scope.csvSend;i++){
            $scope.csvSend[i].splice(0,1);
        }

        var parameters = JSON.stringify(
            {'data':$scope.csvSend,
                'extra':{
                    'lineEditDataTW' : $scope.lineEditDataTW,
                    'lineEditProbTW' : $scope.lineEditProbTW,
                    'lineEditNTW' : $scope.lineEditNTW,
                }});

        $http.post(url+"?type=downloadResult",parameters).then(function (val) {
            console.log(val);
        });
    };

    $scope.clickRUN = function () {
         var url = "http://localhost:3333/server_vetools/hasset.php";
         $scope.csvSend = angular.copy($scope.csvData);
         $scope.csvSend.splice(0,1);
         for(var i=0;i<$scope.csvSend;i++){
             $scope.csvSend[i].splice(0,1);
         }

         var parameters = JSON.stringify(
             {'data':$scope.csvSend,
                 'extra':{
             'lineEditDataTW' : $scope.lineEditDataTW,
             'lineEditProbTW' : $scope.lineEditProbTW,
             'lineEditNTW' : $scope.lineEditNTW
         }});

        $http.post(url+"?type=normal",parameters).then(function (val) {
            $scope.resultCsv = val.data;
            if($scope.resultCsv['error']){
                alert($scope.resultCsv['message'])
            }
            else{
                $scope.tab = 10;
                $scope.result = true;
                $scope.generateUnrest($scope.resultCsv['result']['unrest']);
                $scope.generateHazard($scope.resultCsv['result']['hazard']);
                $scope.restartScenario();
                $scope.scenarioResult = [];

            }
        });
    };
    $scope.restartScenario = function () {
        
        $scope.scenario = {
            "unrest" : "",
            "origin" : "",
            "outcome" : "",
            "location" : "",
            "composition" : "",
            "size" : "",
            "hazard" : {
                "Ballistic" : false,
                "Fallout" : false,
                "PDC" : false,
                "Lava flow" : false,
                "Lahars" : false,
                "Debris avalanche" : false,
                "Other" : false,
            },
            "extent" : ""
        };
    };
    $scope.generateHazard = function (hazard) {
        $scope.hazard = {};
        angular.forEach(hazard, function (value, index) {
            var event = value.event;
            if (!(event in $scope.unrest)) {
                $scope.hazard[event] = {};
            }
            angular.forEach(value, function (value2, index2) {
                if (index2 != 'event' && index2 != 'node name') {
                    $scope.hazard[event][index2] = value2;
                }
            });
        });
        console.log($scope.hazard);
    };
    $scope.generateUnrest = function (unrest) {
        $scope.unrest = {};
        angular.forEach(unrest, function (value, index) {
            if (value.event == 'Yes') {
                angular.forEach(value, function (value2, index2) {
                    if (index2 != 'event') {
                        if (!(index2 in $scope.unrest)) {
                            $scope.unrest[index2] = {}
                        }
                        $scope.unrest[index2]['yes'] = value2;
                    }
                })
            }
            else {
                angular.forEach(value, function (value2, index2) {
                    if (index2 != 'event') {
                        if (!(index2 in $scope.unrest)) {
                            $scope.unrest[index2] = {}
                        }
                        $scope.unrest[index2]['no'] = value2;
                    }
                })
            }
        });
    };



    $scope.toggleMenu = function(){
        if($(".content-menu").hasClass("open-menu")){
            $(".content-menu").removeClass("open-menu").css("opacity","0");
            $(".tabs").css("padding-left","0%");
            $("#pluginMenuView").css({"width":"0px"});
            $timeout(function(){
                $scope.openMenu=false;
            },500)
        }
        else{
            $(".content-menu").addClass("open-menu").css("opacity","1");
            $(".tabs").css("padding-left","20%");
            $("#pluginMenuView").css({"width":"20%"});
            $scope.openMenu=true;
        }
    }
});