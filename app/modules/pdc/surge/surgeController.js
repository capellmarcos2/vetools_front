vetools.controller('surgeController', function($scope,$state,$stateParams,$timeout,vetoolsService,utilService, $http, $uibModal) {
    $scope.pluginSelected = $stateParams.namePlugin;

    $scope.style = 'style="text-align:center; background-color: white; border-radius: 2vw; padding-bottom: 2vh;"';

    $scope.openMenu = true;
    $scope.tab = 1;
    $scope.simulation = 'idle';
    $scope.inputdem = $("#rasterLayerInput").fileinput({showUpload: false});
    $("#rasterLayerSusceptibility").fileinput({showUpload: false});

    $scope.input = {};

    console.log('pdc');

    $scope.selectTab = function (tab,$event) {
        $scope.tab = tab;
        $(".menu-options").removeClass("active");
        $($event.target).addClass("active");
    };

    $scope.calculeAngle = function () {
        console.log('Calculating Angle', $scope.input.collapseHeight === true, $scope.input.collapseHeight);
        if($scope.input.collapseHeight > 0){
           if($scope.input.collapseHeight <= 0 || $scope.input.collapseHeight >= 1300){
                //alert("Collapse height should be greater than 0 and lower than 1300m")
               $scope.modal = $uibModal.open({
                   template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3><span>Collapse height should be greater than 0 and lower than 1300m</span></div>'
               });
           }
           else{
               var a=(Math.log(1300)-Math.log($scope.input.collapseHeight))/0.116;
               $scope.input.collapseAngle = (Math.round(a * 1000) / 1000);
           }
        }
    };

    $scope.toggleMenu = function(){
        if($(".content-menu").hasClass("open-menu")){
            $(".content-menu").removeClass("open-menu").css("opacity","0");
            $(".tabs").css("padding-left","0%");
            $("#pluginMenuView").css({"width":"0px"});
            $timeout(function(){
                $scope.openMenu=false;
            },500)
        }
        else{
            $(".content-menu").addClass("open-menu").css("opacity","1");
            $(".tabs").css("padding-left","20%");
            $("#pluginMenuView").css({"width":"20%"});
            $scope.openMenu=true;
        }
    };

    $scope.runPlugin = function () {

        $scope.simulation = 'running';

        var fileLoaded = $scope.inputdem.fileinput('getFilesCount') > 0;

        if (!fileLoaded) {
            //alert('Missing DEM Raster input file');
            $scope.modal = $uibModal.open({
                template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3> <span>Missing DEM Raster input file</span></div>'
            });
            return;
        }

        var file = $scope.inputdem.fileinput('getFileStack')[0];
        var isSingleVent = $scope.input.isSingleVent;
        var ventCoordinatesX = $scope.input.ventCoordinateX;
        var ventCoordinatesY = $scope.input.ventCoordinateY;
        var collapseHeight = $scope.input.collapseHeight;
        var collapseAngle = $scope.input.collapseAngle;


        var fd = new FormData();
        fd.append('demInput', file);
        fd.append('isSingleVent', isSingleVent);
        fd.append('ventCoordinatesX', ventCoordinatesX);
        fd.append('ventCoordinatesY', ventCoordinatesY);
        fd.append('collapseHeight', collapseHeight);
        fd.append('collapseAngle', collapseAngle);


        $scope.modal = $uibModal.open({
            template: '<div class="col-xs-12" '+$scope.style+'><h3>Running</h3> <span>Simulation is running...</span></div>'
        });
        $http.post("http://198.211.123.50/back/pdc/pdc.php", fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .success(function(data){
                $scope.simulation = 'idle';
                console.log('success', data);
                if (data.result) {
                    $scope.modal.close();
                    $scope.modal = $uibModal.open({
                        template: '<div class="col-xs-12" '+$scope.style+'><h3>Result</h3><span>Simulation has finished</span></div>'
                    });
                    $scope.isResults = true;
                    $scope.outputPreview = data.png;
                    $scope.inputPreview = data.inputPng;
                    $scope.outputDowndload = data.raw;
                    $scope.selectTab(2);
                }
            })
            .error(function(err){
                $scope.simulation = 'idle';
                //alert("The simulation has failed. Error 308");
                $scope.modal.close();
                $scope.modal = $uibModal.open({
                    template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3><span>Request has failed - Error 308</span></div>'
                });
                console.log('error', err);
            });

    }

});