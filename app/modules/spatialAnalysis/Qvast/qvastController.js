vetools.controller('qvastController', function($scope,$state,$stateParams,$timeout, $http, $uibModal, vetoolsService,utilService) {

    $scope.style = 'style="text-align:center; background-color: white; border-radius: 2vw; padding-bottom: 2vh;"';

    $scope.pluginSelected = $stateParams.namePlugin;

    $scope.openMenu = true;
    $scope.tab = 1;

    $scope.inputLeft = $("#inputLeft").fileinput({showUpload: false});
    $scope.inputRight = $("#inputRight").fileinput({showUpload: false});
    $("#demBandwidth").fileinput({showUpload: false});

    $scope.selectTab = function (tab,$event) {
        $scope.tab = tab;
        $(".menu-options").removeClass("active");
        $($event.target).addClass("active");
        $scope.susceptibility.layers = $(".inputPdfLayers").fileinput({showUpload: false});
    };

    $scope.toggleMenu = function(){
        if($(".content-menu").hasClass("open-menu")){
            $(".content-menu").removeClass("open-menu").css("opacity","0");
            $(".tabs").css("padding-left","0%");
            $("#pluginMenuView").css({"width":"0px"});
            $timeout(function(){
                $scope.openMenu=false;
            },500)
        }
        else{
            $(".content-menu").addClass("open-menu").css("opacity","1");
            $(".tabs").css("padding-left","20%");
            $("#pluginMenuView").css({"width":"20%"});
            $scope.openMenu=true;
        }
    };

    $scope.calculateBW = function () {
        if ($scope.bw.method == 'manual') {
            $scope.bw.evaluatedBW = $scope.bw.manualBWValue;
        } else {
            var layer = null;
            // LSCV
            if ($scope.bw.method === 'lscv') {
                // check input file
                if (!($scope.inputLeft.fileinput('getFilesCount') > 0)) {
                    $scope.modal.close();
                    $scope.modal = $uibModal.open({
                        template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3><span>Missing input file</span></div>'
                    });
                    return;
                }
                layer = $scope.inputLeft.fileinput('getFileStack')[0];
            } else
            // hpi silverman
            if (['silverman', 'hpi'].indexOf($scope.bw.method) != -1) {
                if (!($scope.inputRight.fileinput('getFilesCount') > 0)) {
                    $scope.modal.close();
                    $scope.modal = $uibModal.open({
                        template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3><span>Missing input file</span></div>'
                    });
                    return;
                }
                layer = $scope.inputRight.fileinput('getFileStack')[0];
            }

            var fd = new FormData();
            fd.append('bwInput', layer);
            fd.append('method', $scope.bw.method);

            $scope.modal = $uibModal.open({
                template: '<div class="col-xs-12" '+$scope.style+'><h3>Running</h3> <span>Calculating bandwidth...</span></div>'
            });
            $http.post("http://198.211.123.50/back/qvast/qvast_bw.php", fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(data){
                    $scope.simulation = 'idle';
                    console.log('success', data);
                    //clearInterval($scope.interval);
                    //$scope.perecent = 100;
                    $scope.modal.close();
                    $scope.modal = $uibModal.open({
                        template: '<div class="col-xs-12" '+$scope.style+'><h3>Result</h3><span>Bandwidth calculated</span></div>'
                    });
                    $scope.isResults = true;
                    $scope.bw.evaluatedBW = data.bw;

                })
                .error(function(err){
                    $scope.simulation = 'idle';
                    $scope.modal.close();
                    $scope.modal = $uibModal.open({
                        template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3><span>Request failed. Error 308</span></div>'
                    });
                    //alert("The simulation has failed. Error 308");
                    console.log('error', err);
                });


        }
    };
    
    $scope.generatePDF = function () {
        if ($scope.pdf.north <= $scope.pdf.south || $scope.pdf.east >= $scope.pdf.west) {
            $uibModal.open({
                template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3> <span>Extent is not correct</span></div>'
            });
            return;
        }

        if ($scope.pdf.resolution <= 0) {
            $uibModal.open({
                template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3> <span>Resolution value is not correct</span></div>'
            });
            return;
        }

        if ($scope.pdf.resolution > ($scope.pdf.north - $scope.pdf.south) || $scope.pdf.resolution > ($scope.pdf.west - $scope.pdf.east)) {
            $uibModal.open({
                template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3> <span>Resolution value is greater than extent</span></div>'
            });
            return;
        }

        if (!$scope.bw.evaluatedBW || $scope.bw.evaluatedBW == 0) {
            $uibModal.open({
                template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3> <span>Bandwidth value is not correct</span></div>'
            });
            return;
        }

        var fd = new FormData();
        fd.append('N', $scope.pdf.north);
        fd.append('S', $scope.pdf.south);
        fd.append('W', $scope.pdf.west);
        fd.append('E', $scope.pdf.east);
        fd.append('R', $scope.pdf.resolution);
        fd.append('H', $scope.bw.evaluatedBW);
        fd.append('P', 'T');

        $scope.modal = $uibModal.open({
            template: '<div class="col-xs-12" '+$scope.style+'><h3>Running</h3> <span>Simulation is running...</span></div>'
        });
        $http.post("http://198.211.123.50/back/qvast/qvast_pdf.php", fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .success(function(data){
                $scope.simulation = 'idle';
                if(data.success) {
                    console.log('success', data);
                    $scope.modal.close();
                    $scope.modal = $uibModal.open({
                        template: '<div class="col-xs-12" '+$scope.style+'><h3>Result</h3><a href="'+data.out+'">Result (GeoTiff)</a></div>'
                    });
                    //alert("The PDF generation has finished");
                    $scope.pdf.result = true;
                    $scope.pdf.out = data.out;
                } else {
                    $scope.modal.close();
                    $scope.modal = $uibModal.open({
                        template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3><span>PDF generation failed. QVAST-101</span></div>'
                    });
                    //alert("The PDF generation failed. Error QVAST-002");
                }
            })
            .error(function(err){
                $scope.simulation = 'idle';
                $scope.modal.close();
                $scope.modal = $uibModal.open({
                    template: '<div class="col-xs-12" '+$scope.style+'><h1>Error</h1><span>Request failed. Error 308</span></div>'
                });
                //alert("The simulation has failed. Error 308");
                console.log('error', err);
            });

    };

    $scope.calculateSusceptibility = function() {

        console.log($scope.susceptibility);

        var numOfLayers = $scope.layers.length;

        if (!($scope.susceptibility.layers.fileinput('getFilesCount').length > 1)) {
            $scope.modal = $uibModal.open({
                template: '<div class="col-xs-12" '+$scope.style+'><h1>Error</h1><span>There must be more than one layer</span></div>'
            });
            return;
        }

        if ($scope.susceptibility.resolution <= 0) {
            $scope.modal = $uibModal.open({
                template: '<div class="col-xs-12" '+$scope.style+'><h1>Error</h1><span>Resolution value must be greater than 0</span></div>'
            });
            return;
        }

        var totalWeight = 0;
        for(var j=0; j<$scope.susceptibility.weight.length; j++) {
            if (!$scope.susceptibility.weight[j]) {
                $scope.modal = $uibModal.open({
                    template: '<div class="col-xs-12" '+$scope.style+'><h1>Error</h1><span>All layer\'s weights must be informed</span></div>'
                });
                return;
            }
            totalWeight += parseFloat($scope.susceptibility.weight[j]);
        }

        if (totalWeight != 1) {
            $scope.modal = $uibModal.open({
                template: '<div class="col-xs-12" '+$scope.style+'><h1>Error</h1><span>All layer\'s weights must sum 1</span></div>'
            });
            return;
        }

        var fd = new FormData();
        fd.append('numLayers', numOfLayers);
        fd.append('resolution', $scope.susceptibility.resolution);

        for(var i = 0; i < $scope.susceptibility.layers.fileinput('getFilesCount').length; i++) {
            fd.append('layer'+i, $scope.susceptibility.layers.fileinput('getFileStack')[i]);
        }

        $scope.modal = $uibModal.open({
            template: '<div class="col-xs-12" '+$scope.style+'><h3>Running</h3> <span>Operation is running</span></div>'
        });

        $http.post("http://198.211.123.50/back/qvast/qvast_susceptibility.php", fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .success(function(data){
                $scope.simulation = 'idle';
                console.log('success', data);
                if (data.success) {
                    $scope.modal.close();
                    $scope.modal = $uibModal.open({
                        template: '<div class="col-xs-12" ' + $scope.style + '><h3>Result</h3><a href="' + data.out + '">Result (GeoTiff)</a></div>'
                    });
                    $scope.isResults = true;
                } else {
                    $scope.modal.close();
                    $scope.modal = $uibModal.open({
                        template: '<div class="col-xs-12" ' + $scope.style + '><h3>Error</h3><span>Susceptibility calculation failed. QVAST-203</span></div>'
                    });
                }
            })
            .error(function(err){
                $scope.simulation = 'idle';
                $scope.modal.close();
                $scope.modal = $uibModal.open({
                    template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3><span>Request failed. Error 308</span></div>'
                });
                console.log('error', err);
            });


    };

    $scope.addLayer = function () {
        $scope.layers.push($scope.layers.length);
        $timeout(function() {
            $scope.susceptibility.layers = $(".inputPdfLayers").fileinput({showUpload: false});
        }, 300);
    };

    $scope.removeLayer = function () {
        $scope.layers.pop();
    };
    
    var initNgModels = function () {
        $scope.bw = {
            method: false,
            manualBW: false,
            manualBWValue: 0,
            evaluatedBW: 0
        };

        $scope.pdf = {
            north: 0,
            south: 0,
            east: 0,
            west: 0,
            resolution: 0,
        };

        $scope.layers = [0];

        $scope.susceptibility = {
            layers: null,
            resolution: 0,
            weight: []
        };
    };

    initNgModels();

});