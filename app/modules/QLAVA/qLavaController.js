/**
 * Created by alexmarcos on 6/3/16.
 */
vetools.controller('QLAVAController', function($scope,$state,$stateParams,$timeout,vetoolsService,utilService, $uibModal) {

    $scope.pluginSelected = $stateParams.namePlugin;

    $scope.style = 'style="text-align:center; background-color: white; border-radius: 2vw; padding-bottom: 2vh;"';

    //todo: create from directive
    $scope.openMenu = true;
    $scope.tab = 1;
    $("#lineEditImportInputsPath").fileinput({showUpload: false});
    $("#lineEditImportInputsSavePath").fileinput({showUpload: false});
    $("#lineEditDEM").fileinput({showUpload: false});
    $("#lineEditFI").fileinput({showUpload: false});
    $("#lineEditOutputPath").fileinput({showUpload: false});
    $("#lineEditFile").fileinput({showUpload: false});

    $scope.utils = utilService;
    $scope.typeVent = utilService.TYPE_VENT_POINT;
    $scope.checkBoxPDF = utilService.CHECKBOX_VALUE_FALSE;
    $scope.fintessCalculator = utilService.CHECKBOX_VALUE_FALSE;
    $scope.lavaFlowLength = utilService.LAVA_FLOW_LENGTH_MAX;
    $scope.advancedParametersFlowgo = utilService.CHECKBOX_VALUE_FALSE;

    $timeout(function () {

        $scope.$watch('typeVent',function () {
            if($scope.typeVent === utilService.TYPE_VENT_SURFACE){
                $("#checkBoxPDF").removeAttr("disabled");

            }
            else{
                $("#checkBoxPDF").attr("disabled", true);
                $("#lineEditFile").attr("disabled", true);
                $("#lineEditFile").fileinput('refresh');
            }
        });
        $scope.$watch('advancedParametersFlowgo',function () {
            if($scope.advancedParametersFlowgo == utilService.CHECKBOX_VALUE_TRUE){
                $("."+utilService.CLASS_ADVANDED_FLOWGO).removeAttr("disabled");
            }
            else{
                $("."+utilService.CLASS_ADVANDED_FLOWGO).attr("disabled", true);
            }
        });
        $scope.$watch('fintessCalculator',function () {
            if($scope.fintessCalculator == utilService.CHECKBOX_VALUE_TRUE){
                $("#lineEditFI").removeAttr("disabled");
            }
            else{
                $("#lineEditFI").attr("disabled", true);
            }
            $("#lineEditFI").fileinput('refresh');
        });

        $scope.$watch('checkBoxPDF',function () {
            console.log($scope.checkBoxPDF);
            if($scope.checkBoxPDF == utilService.CHECKBOX_VALUE_TRUE){
                $("#lineEditFile").removeAttr("disabled");
            }
            else{
                $("#lineEditFile").attr("disabled", true);
            }
            $("#lineEditFile").fileinput('refresh');
        });


    });


    $scope.clickRUN = function () {
        var fd = new FormData();
        $("."+utilService.CHECKBOX_CLASS).each(function (index,obj) {
            var jObj = $(obj);
            if(jObj.is(":checked")){
                fd.append(jObj.attr("id"),1);
            }
            else{
                fd.append(jObj.attr("id"),0);
            }
        });
        $("."+utilService.RADIO_CLASS).each(function (index,obj) {
            var jObj = $(obj);
            if(jObj.is(":checked")){
                fd.append(jObj.attr("id"),1);
            }
            else{
                fd.append(jObj.attr("id"),0);
            }
        });

        $(".plugin-input").each(function (index,obj) {
            var jObj = $(obj);
            if(jObj.val() != "")
                fd.append(jObj.attr("id"),jObj.val());
        });
        $(".file-input").each(function (index,obj) {
            var jObj = $(obj);
            if(jObj.val() != "")
                fd.append(jObj.attr("id"), jObj.fileinput('getFileStack')[0]);
                //result[jObj.attr("id")] = fd;
        });

        $scope.lavaFlowParameters = [];
        $scope.lavaFlowParameters[utilService.LAVA_FLOW_LENGTH_MAX] = function () {
            $("."+utilService.CLASS_ADVANCED_MAX).each(function (index,obj) {
                var jObj = $(obj);
                if(jObj.val() != "")
                    fd.append(jObj.attr("id"),jObj.val());
            });
        };
        $scope.lavaFlowParameters[utilService.LAVA_FLOW_LENGTH_DECREASING] = function () {
            $("."+utilService.CLASS_ADVANCED_DECREASING).each(function (index,obj) {
                var jObj = $(obj);
                if(jObj.val() != "")
                    fd.append(jObj.attr("id"),jObj.val());
            });
        };
        $scope.lavaFlowParameters[utilService.LAVA_FLOW_LENGTH_FLOWGO] = function () {
            $("."+utilService.CLASS_NORMAL_FLOWGO).each(function (index,obj) {
                var jObj = $(obj);
                if(jObj.val() != "")
                    result[jObj.attr("id")] = jObj.val();
            });
            if($scope.advancedParametersFlowgo){
                $("."+utilService.CLASS_ADVANDED_FLOWGO).each(function (index,obj) {
                    var jObj = $(obj);
                    if(jObj.val() != "")
                        result[jObj.attr("id")] = jObj.val();
                });
            }
        };

        $scope.lavaFlowParameters[$scope.lavaFlowLength]();


        var url = "http://198.211.123.50/back/qlavha/qlavha.php";
        $scope.modal = $uibModal.open({
            template: '<div class="col-xs-12" '+$scope.style+'><h3>Running</h3><span>Simulation is running...</span></div>'
        });
        vetoolsService.callPlugin(url,fd).then(function (val) {
            if (val.success) {
                $scope.modal.close();
                $scope.modal = $uibModal.open({
                    template: '<div class="col-xs-12" '+$scope.style+'><h3>Result</h3><span>Simulation has finished</span></div>'
                });
                $scope.selectTab(4);
            } else {
                $scope.modal.close();
                $scope.modal = $uibModal.open({
                    template: '<div class="col-xs-12" '+$scope.style+'><h3>Error</h3><span>Simulation has failed - QLavHA 105</span></div>'
                });
            }
        })
    };
    $scope.selectTab = function (tab,$event) {
        $scope.tab = tab;
        $(".menu-options").removeClass("active");
        $($event.target).addClass("active");
    };
    $scope.toggleMenu = function(){
        if($(".content-menu").hasClass("open-menu")){
            $(".content-menu").removeClass("open-menu").css("opacity","0");
            $(".tabs").css("padding-left","0%");
            $("#pluginMenuView").css({"width":"0px"});
            $timeout(function(){
                $scope.openMenu=false;
            },500)
        }
        else{
            $(".content-menu").addClass("open-menu").css("opacity","1");
            $(".tabs").css("padding-left","20%");
            $("#pluginMenuView").css({"width":"20%"});
            $scope.openMenu=true;
        }
    }

});