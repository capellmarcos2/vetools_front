/**
 * Created by alexmarcos on 6/3/16.
 */
vetools.controller('hassetStController', function($scope,$state,$stateParams,$timeout,vetoolsService,utilService) {

    $scope.pluginSelected = $stateParams.namePlugin;


    //todo: create from directive
    $scope.openMenu = true;
    $scope.tab = 1;
    $scope.result = false;
    $("#linePointsPath").fileinput({showUpload: false});

    $scope.utils = utilService;
    $scope.csvData = [];
    $scope.selectTab = function (tab,$event) {
        $scope.tab = tab;
        $(".menu-options").removeClass("active");
        $($event.target).addClass("active");
    };
    $("#linePointsPath").on('fileloaded', function(event, file, previewId, index, reader) {
        var csvFile = $(this).fileinput('getFileStack')[0];
        reader.readAsText(file);
        reader.onload = function(event) {
            var csvData = event.target.result;
            $scope.csvData = $.csv.toArrays(csvData);
            console.log($scope.csvData);
            $scope.$apply();
        };
        reader.onerror = function() {
            alert('Unable to read ' + file.fileName);
        };

        console.log(event, file, previewId, index, reader,csvFile);
    });

    $scope.clickRUN = function () {
        var fd = new FormData();
        $(".plugin-input").each(function (index,obj) {
            var jObj = $(obj);
            if(jObj.val() != "")
                fd.append(jObj.attr("id"),jObj.val());
        });

        fd.append("data",$scope.csvData);

        console.log(fd);

        $scope.tab = 10;
        $scope.result = true;
        $scope.scenario = {
            "unrest" : "",
            "origin" : "",
            "outcome" : "",
            "location" : "",
            "composition" : "",
            "size" : "",
            "hazard" : {
                "ballistic" : false,
                "fallout" : false,
                "pdc" : false,
                "lava" : false,
                "lahar" : false,
                "debris" : false,
                "other" : false,
            },
            "extent" : "",
        };
        $scope.hazard = {
            "BALLISTIC" : {
                "pastData":2,
                "prioriWeight":2,
                "dataWeight":2,
                "probabilityEstitmate":2,
                "standardDeviation":2,
            },
            "FALLOUT" : {
                "pastData":2,
                "prioriWeight":2,
                "dataWeight":2,
                "probabilityEstitmate":2,
                "standardDeviation":2,
            },
            "PDC" : {
                "pastData":2,
                "prioriWeight":2,
                "dataWeight":2,
                "probabilityEstitmate":2,
                "standardDeviation":2,
            },
            "LAVA FLOW" : {
                "pastData":2,
                "prioriWeight":2,
                "dataWeight":2,
                "probabilityEstitmate":2,
                "standardDeviation":2,
            },
            "LAHARS" : {
                "pastData":2,
                "prioriWeight":2,
                "dataWeight":2,
                "probabilityEstitmate":2,
                "standardDeviation":2,
            },
            "DEBRIS AVALANCHE" : {
                "pastData":2,
                "prioriWeight":2,
                "dataWeight":2,
                "probabilityEstitmate":2,
                "standardDeviation":2,
            },
            "OTHER" : {
                "pastData":2,
                "prioriWeight":2,
                "dataWeight":2,
                "probabilityEstitmate":2,
                "standardDeviation":2,
            }
        };
        $scope.unrest = {
            "pastData":{
                "yes" : 18,
                "no": 62
            },
            "prioriWeight":{
                "yes" : 18,
                "no": 62
            },
            "dataWeight":{
                "yes" : 18,
                "no": 62
            },
            "probabilityEstitate":{
                "yes": 0.44,
                "no" : 0.55
            },
            "standardDeviation":{
                "yes": 0.44,
                "no" : 0.55
            }
        };
        var url = "";
    };

    $scope.toggleMenu = function(){
        if($(".content-menu").hasClass("open-menu")){
            $(".content-menu").removeClass("open-menu").css("opacity","0");
            $(".tabs").css("padding-left","0%");
            $("#pluginMenuView").css({"width":"0px"});
            $timeout(function(){
                $scope.openMenu=false;
            },500)
        }
        else{
            $(".content-menu").addClass("open-menu").css("opacity","1");
            $(".tabs").css("padding-left","20%");
            $("#pluginMenuView").css({"width":"20%"});
            $scope.openMenu=true;
        }
    }
});