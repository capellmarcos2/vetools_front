vetools.controller('referencesController', function($scope,$state,$stateParams,$timeout,vetoolsService,utilService) {
    $scope.pluginSelected = $stateParams.namePlugin;

    $scope.openMenu = true;
    $scope.tab = 1;

    $scope.selectTab = function (tab,$event) {
        $scope.tab = tab;
        $(".menu-options").removeClass("active");
        $($event.target).addClass("active");
    };

    $scope.toggleMenu = function(){
        if($(".content-menu").hasClass("open-menu")){
            $(".content-menu").removeClass("open-menu").css("opacity","0");
            $(".tabs").css("padding-left","0%");
            $("#pluginMenuView").css({"width":"0px"});
            $timeout(function(){
                $scope.openMenu=false;
            },500)
        }
        else{
            $(".content-menu").addClass("open-menu").css("opacity","1");
            $(".tabs").css("padding-left","20%");
            $("#pluginMenuView").css({"width":"20%"});
            $scope.openMenu=true;
        }
    }
});