/**
 * Created by alexmarcos on 6/3/16.
 */

vetools.config(function ($stateProvider,$urlRouterProvider) {


    $stateProvider
        .state('index', {
            views: {
                '@': {
                    templateUrl: "app/components/abstract/abstractView.html"
                },
                "headerView@index": {
                    templateUrl: "app/modules/header/header.html",
                    controller: "headerController",
                    replace : true
                }
            }

        })
        .state('home', {
            parent:"index",
            url: '/',
            views: {
                "homeView@index": {
                    templateUrl: "app/components/home/home.html",
                    controller: "homeController"

                }
            }

        })
        .state('pluginAbstract', {
            parent:"index",
            abstract:true,
            url: '/plugin',
            views: {
                "homeView@index": {
                    templateUrl: "app/components/abstractPlugin/abstractPluginView.html",
                }
            }

        })
        .state('qlavha', {
            parent:"pluginAbstract",
            url: '/lava/qlavha',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/QLAVA/QLAVA.html",
                    controller: "QLAVAController"
                }
            }

        })
        .state('hasset', {
            parent:"pluginAbstract",
            url: '/hasset/hasset',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/hasset/hasset.html",
                    controller: "hassetController"
                }
            }
        })
        .state('hazmap', {
            parent: "pluginAbstract",
            url: 'ashfall/hazmap',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/ashfall/hazmap/hazmap.html",
                    controller: "hazmapController"
                }
            }
        })
        .state('fall3d', {
            parent: "pluginAbstract",
            url: 'ashfall/fall3d',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/ashfall/fall3d/fall3d.html",
                    controller: "fall3dController"
                }
            }
        })
        .state('surge', {
            parent: "pluginAbstract",
            url: 'pdc/surge',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/pdc/surge/surge.html",
                    controller: "surgeController"
                }
            }
        })
        .state('qvast', {
            parent: "pluginAbstract",
            url: 'spatialAnalysis/qvast',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/spatialAnalysis/Qvast/qvast.html",
                    controller: "qvastController"
                }
            }
        })
        .state('laharzPlugin', {
            parent: "pluginAbstract",
            url: 'laharz',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/laharz/laharz.html",
                    controller: "laharzController"
                }
            }
        })
        .state('bademo', {
            parent: "pluginAbstract",
            url: 'temporalAnalysis/bademo',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/temporalAnalysis/bademo/bademo.html",
                    controller: "bademoController"
                }
            }
        })
        .state('hasset-st', {
            parent: "pluginAbstract",
            url: 'temporalAnalysis/hasset-st',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/hassetSt/hassetSt.html",
                    controller: "hassetStController"
                }
            }
        })
        .state('volcdam', {
            parent: "pluginAbstract",
            url: 'costbenefit/volcdam',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/costBenefits/volcdam/volcdam.html",
                    controller: "volcdamController"
                }
            }
        })
        .state('credits', {
            parent: "pluginAbstract",
            url: 'doc/credits',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/doc/credits/credits.html",
                    controller: "creditsController"
                }
            }
        })
        .state('manuals', {
            parent: "pluginAbstract",
            url: 'doc/manuals',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/doc/manuals/manuals.html",
                    controller: "manualsController"
                }
            }
        })
        .state('references', {
            parent: "pluginAbstract",
            url: 'doc/references',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/doc/references/references.html",
                    controller: "referencesController"
                }
            }
        })
        .state('tutorials', {
            parent: "pluginAbstract",
            url: 'doc/tutorials',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/doc/tutorials/tutorials.html",
                    controller: "tutorialsController"
                }
            }
        })
        .state('landslides', {
            parent: "pluginAbstract",
            url: 'others/landslides',
            views: {
                "pluginContent@pluginAbstract": {
                    templateUrl: "app/modules/landslides/landslides.html",
                    controller: "landslidesController"
                }
            }
        });


    $urlRouterProvider.otherwise('/');

});